package model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database {

    private HashMap<Integer, Book> books;
    private Object[][] booksConverted;

    /**
     * Kinda "database" to operate with data
     */
    public Database() {
        this.books = new HashMap<>();
    }

    /**
     * Removing row from db's hashmap by book id
     *
     * @param bookId
     */
    public void removeRow(int bookId) {
        books.remove(bookId);
    }

    /**
     * Getting vector of books converted from list
     *
     * @return
     */
    public Object[][] getBooks() {
        convertBooks();
        return booksConverted;
    }

    /**
     * Converting list to object[][]
     */
    public void convertBooks() {
        Object[][] array = new Object[books.size()][];
        int i = 0;

        for (Map.Entry<Integer, Book> entry : books.entrySet()) {
            if (entry.getValue().getPublishDate() == null) {
                Object[] obj = new Object[]{
                        entry.getValue().getBookId(),
                        entry.getValue().getBookName(),
                        entry.getValue().getISBN(),
                        entry.getValue().getBookShelf(),
                        entry.getValue().getSerialNumber(),
                        ""
                };

                array[i] = obj;
            } else {
                Object[] obj = new Object[]{
                        entry.getValue().getBookId(),
                        entry.getValue().getBookName(),
                        entry.getValue().getISBN(),
                        entry.getValue().getBookShelf(),
                        entry.getValue().getSerialNumber(),
                        entry.getValue().getPublishDate()
                };

                array[i] = obj;
            }

            i++;
        }

        this.booksConverted = array;
    }

    /**
     * Setting db's hashmap with books
     *
     * @param books
     */
    public void setBooks(List<Book> books) {
        this.books.clear();

        for (Book b : books) {
            this.books.put(b.getBookId(), b);
        }
    }

    public int getSize() {
        return this.books.size();
    }
}
