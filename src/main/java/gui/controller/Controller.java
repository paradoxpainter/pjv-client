package gui.controller;

import connection.ClientConnection;
import gui.frames.BookListFrame;
import gui.frames.BorrowListFrame;
import model.Book;
import model.Database;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class Controller {

    private static BookListFrame bookList = null;
    private static BorrowListFrame borrowList = null;

    private static Database db = new Database();
    private static Database dbAvailable = new Database();
    private static Database dbBorrowed = new Database();
    private static int userID;

    public static Object[][] getAvailableBooks() {
        return dbAvailable.getBooks();
    }

    public static Object[][] getBorrowedBooks() {
        return dbBorrowed.getBooks();
    }

    /**
     * Setting internal data from server
     *
     * @param books list of Books
     */
    public static void setDb(List<Book> books) {
        db.setBooks(books);
        List<Book> available =books.stream().filter(book -> book.getBorrowUserId() == null).collect(Collectors.toList());
        List<Book> borrowed =books.stream().filter(book -> book.getBorrowUserId() != null).collect(Collectors.toList());
        dbAvailable.setBooks(available);
        dbBorrowed.setBooks(borrowed);
    }

    public Database getDb() {
        return db;
    }

    public static int getUserID() {
        return userID;
    }

    public static void setUserID(int userID) {
        Controller.userID = userID;
    }

    /**
     * Method will check correctness of user's login data on the server
     *
     * @param username
     * @param password
     * @param address
     * @param port
     * @return true if login data is correct, false otherwise
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static boolean isCorrectPassword(String username, String password, String address, int port) throws IOException, ClassNotFoundException {
        if (ClientConnection.isSocketAlive(address)) {
            ClientConnection.connect(address, port);
            ClientConnection.setupStreams();
            if (ClientConnection.passwordCheck(username, password)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static BorrowListFrame getBorrowList() {
        return borrowList;
    }

    public static void setBorrowList(BorrowListFrame frame) {
        borrowList = frame;
    }

    /**
     * Creating or setting BookList visible
     */
    public static void openBookList() {
        if (bookList != null) {
            showBookList();
        } else {
            bookList = new BookListFrame();
            bookList.setVisible(true);
        }
    }

    /**
     * Setting BookList visible
     */
    public static void showBookList() {
        bookList.setVisible(true);
    }

    /**
     * Setting BookList invisible
     */
    public static void hideBookList() {
        bookList.setVisible(false);
    }

    /**
     * Creating or setting BorrowList visible
     *
     * @return borrowList field of Controller
     */
    public static BorrowListFrame openBorrowList() throws IOException, ClassNotFoundException {
        if (borrowList != null) {
            showBorrowList();
        } else {
            borrowList = new BorrowListFrame();
            borrowList.setVisible(true);
        }

        return borrowList;
    }

    /**
     * Setting BorrowList visible
     */
    public static void showBorrowList() {
        borrowList.setVisible(true);
    }

    /**
     * Hiding BorrowList
     */
    public static void hideBorrowList() {
        borrowList.setVisible(false);
    }

    /**
     * Sending request to the server for borrowing book
     *
     * @param bookId
     * @throws IOException
     */
    public static void borrowBook(int bookId) throws IOException {
        ClientConnection.sendRequest("BORROW " + userID + " " + bookId);
        db.removeRow(bookId);
    }

    /**
     * Sending request to the server for returning book
     *
     * @param bookId
     * @throws IOException
     */
    public static void returnBook(int bookId) throws IOException {
        ClientConnection.sendRequest("RETURN " + userID + " " + bookId);
        db.removeRow(bookId);
    }
}
