package gui.components;

import connection.ClientConnection;
import gui.controller.Controller;
import gui.listeners.GoFrameListener;
import gui.listeners.RefreshTableListener;
import model.Book;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class BookListPanel extends JPanel {

    private final static Logger LOGGER = Logger.getLogger(BookListPanel.class.getName());

    private JButton refreshTableButton;
    private JButton borrowButton;
    private JButton exitButton;

    private GoFrameListener goFrameListener;
    private RefreshTableListener refreshTableListener;

    public BookListPanel() {
        Dimension dim = getPreferredSize();

        dim.width = 250;
        setPreferredSize(dim);

        refreshTableButton = new JButton("Refresh Table");
        borrowButton = new JButton("Borrowed List");
        exitButton = new JButton("Exit");

        // ====== Set up buttons ======

        refreshTableButton.setMnemonic(KeyEvent.VK_R);
        borrowButton.setMnemonic(KeyEvent.VK_B);
        exitButton.setMnemonic(KeyEvent.VK_X);

        borrowButton.addActionListener(e -> {
            if (refreshTableListener != null) {
                try {
                    List<Book> books = ClientConnection.sendBookRequest("ALL " + Controller.getUserID());
                    Controller.setDb(books);
                } catch (IOException ex) {
                    LOGGER.severe("CLIENT: Error while sending message to the server!");
                } catch (ClassNotFoundException ex) {
                    LOGGER.severe("CLIENT: There's no such class Book found");
                }
                refreshTableListener.refreshBookTable();
            }

            if (goFrameListener != null) {
                goFrameListener.moveToFrame(e);
            }
        });

        // ====== Set listener on top of Exit button ======

        exitButton.addActionListener(e -> {
            try {
                ClientConnection.sendRequest("QUIT");
                ClientConnection.disconnect();
                ClientConnection.closeStreams();
            } catch (IOException ex) {
                LOGGER.severe("CLIENT: Connection was lost");
            }
            System.exit(0);
        });

        // ====== Set listener on top of Refresh button ======

        refreshTableButton.addActionListener(e -> {
            if (refreshTableListener != null) {
                try {
                    List<Book> books = ClientConnection.sendBookRequest("ALL " + Controller.getUserID());
                    Controller.setDb(books);
                } catch (IOException ex) {
                    LOGGER.severe("CLIENT: Error while sending message to the server!");
                } catch (ClassNotFoundException ex) {
                    LOGGER.severe("CLIENT: There's no such class Book found");
                }
                refreshTableListener.refreshBookTable();
            }
        });

        Border innerBorder = BorderFactory.createTitledBorder("List of Available Books");
        Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));

        layoutComponents();
    }

    private void layoutComponents() {
        JPanel controlsPanel = new JPanel();
        JPanel buttonsPanel1 = new JPanel();
        JPanel buttonsPanel2 = new JPanel();
        JPanel buttonsPanel3 = new JPanel();

        controlsPanel.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();

        gc.gridy = 0;
        Insets rightPadding = new Insets(15, 0, 15, 15);
        Insets noPadding = new Insets(0, 0, 0, 0);

        // ====== First row ======

        gc.weighty = 1;
        gc.weightx = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;

        // ====== Next row ======

        gc.gridy++;

        gc.weighty = 1;
        gc.weightx = 1;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;

        // ====== Next row ======

        gc.gridy++;

        gc.weighty = 1;
        gc.weightx = 2;
        gc.fill = GridBagConstraints.NONE;

        gc.gridx = 0;
        gc.anchor = GridBagConstraints.EAST;
        gc.insets = rightPadding;

        gc.gridx++;
        gc.anchor = GridBagConstraints.WEST;
        gc.insets = noPadding;

        // ====== Buttons Panel ======

        buttonsPanel1.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonsPanel2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        buttonsPanel3.setLayout(new FlowLayout(FlowLayout.CENTER));
        buttonsPanel1.add(refreshTableButton);
        buttonsPanel2.add(borrowButton);
        buttonsPanel3.add(exitButton);

        // ====== Add sub panels to dialog ======

        setLayout(new BorderLayout());
        add(controlsPanel, BorderLayout.NORTH);
        add(buttonsPanel1, BorderLayout.EAST);
        add(buttonsPanel2, BorderLayout.WEST);
        add(buttonsPanel3, BorderLayout.SOUTH);
    }

    /**
     * Setting listener which will transfer to another frame
     *
     * @param listener
     */
    public void setGoFrameListener(GoFrameListener listener) {
        this.goFrameListener = listener;
    }

    /**
     * Setting listener which will refresh table
     *
     * @param listener
     */
    public void setRefreshTableListener(RefreshTableListener listener) {
        this.refreshTableListener = listener;
    }
}


