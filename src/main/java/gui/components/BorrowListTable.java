package gui.components;

import connection.ClientConnection;
import gui.controller.Controller;
import gui.listeners.BookTableListener;
import model.BookTableModel;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.logging.Logger;

public class BorrowListTable extends JPanel {

    private final static Logger LOGGER = Logger.getLogger(BorrowListTable.class.getName());

    private JTable table;
    private JPopupMenu popupMenu;
    private BookTableModel bookModel;
    private TableRowSorter<TableModel> rowSorter;
    private JTextField searchField;

    private BookTableListener bookTableListener;

    public BorrowListTable() {

        bookModel = new BookTableModel(Controller.getBorrowedBooks());
        table = new JTable(bookModel);
        rowSorter = new TableRowSorter<>(table.getModel());
        popupMenu = new JPopupMenu();
        searchField = new JTextField();

        TableColumnModel tcm = table.getColumnModel();
        tcm.removeColumn(tcm.getColumn(0));

        table.setRowSorter(rowSorter);

        JPanel panel = new JPanel(new BorderLayout());

        panel.add(new JLabel("Search: "), BorderLayout.WEST);
        panel.add(searchField, BorderLayout.CENTER);

        table.getTableHeader().setReorderingAllowed(false);

        setLayout(new BorderLayout());
        add(panel, BorderLayout.SOUTH);
        add(new JScrollPane(table), BorderLayout.CENTER);

        // ====== Set up Row Filter ======

        searchField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = searchField.getText();

                if (text.trim().length() == 0) {
                    LOGGER.fine("CLIENT: null insert into SEARCH field");
                    rowSorter.setRowFilter(null);
                } else {
                    LOGGER.fine("CLIENT: insert into SEERCH field");
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = searchField.getText();

                if (text.trim().length() == 0) {
                    LOGGER.fine("CLIENT: null remove from SEARCH field");
                    rowSorter.setRowFilter(null);
                } else {
                    LOGGER.fine("CLIENT: remove from SEARCH field");
                    rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text));
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                LOGGER.fine("CLIENT: change SEARCH field");
            }
        });

        // ====== Set up MenuPopup Items ======

        JMenuItem returnItem = new JMenuItem("Return book");
        popupMenu.add(returnItem);

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());

                table.getSelectionModel().setSelectionInterval(row, row);

                if (e.getButton() == MouseEvent.BUTTON3) {
                    popupMenu.show(table, e.getX(), e.getY());
                }
            }
        });

        returnItem.addActionListener(e -> {
            int row = table.convertRowIndexToModel(table.getSelectedRow());

            if (bookTableListener != null) {
                int bookId = (int) bookModel.getValueAt(row, 0);
                bookTableListener.rowReturned(bookId);
                bookModel.removeRow(row);

//                try {
//                    this.refresh();
//                } catch (Exception ex) {
//                    LOGGER.severe("CLIENT: Can not connect to the server!");
//                }
            }
        });

        Border innerBorder = BorderFactory.createEmptyBorder();
        Border outerBorder = BorderFactory.createEmptyBorder(5, 5, 5, 5);
        setBorder(BorderFactory.createCompoundBorder(outerBorder, innerBorder));

    }

    /**
     * Refreshing the table
     */
    public void refresh() throws IOException, ClassNotFoundException {
//        Controller.setDb(ClientConnection.sendBookRequest("ALL " + Controller.getUserID()));
        bookModel.setData(Controller.getBorrowedBooks());
        bookModel.fireTableDataChanged();
    }

    /**
     * Listener which sorts table data
     *
     * @param listener
     */
    public void setBookTableListener(BookTableListener listener) {
        this.bookTableListener = listener;
    }
}
