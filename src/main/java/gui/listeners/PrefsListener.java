package gui.listeners;

public interface PrefsListener {

    /**
     * Listener to set your default data on login frame
     *
     * @param username
     * @param password
     * @param address
     * @param port
     */
    public void preferencesSet(String username, String password, String address, int port);
}
