package gui.listeners;

import gui.events.LoginEvent;

import java.io.IOException;
import java.util.EventListener;

public interface LoginListener extends EventListener {

    /**
     * Sending login event with login data to controller
     *
     * @param event
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void loginEventOccured(LoginEvent event) throws IOException, ClassNotFoundException;
}
