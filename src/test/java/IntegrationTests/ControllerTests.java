package IntegrationTests;

import gui.controller.Controller;
import model.Book;
import org.junit.jupiter.api.Test;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static Data.DataGenerator.*;
import static org.junit.jupiter.api.Assertions.*;

public class ControllerTests {

    @Test
    public void GetBooksTest() {
        // Arrange
        Controller controller = new Controller();
        String expectedName = GenerateString(7);
        String expectedShelf = GenerateString(7);
        String expectedISBN = GenerateString(7);
        String expectedSerialNumber = GenerateString(7);
        Date expectedDate = new Date(GenerateLong());
        Integer expectedUserId = GenerateInt();
        Book book =
                new Book(
                        GenerateInt(),
                        expectedName,
                        expectedISBN,
                        expectedShelf,
                        expectedSerialNumber,
                        expectedDate,
                        expectedUserId
                );
        List<Book> books = new ArrayList<>();
        books.add(book);
        controller.setDb(books);

        // Act
        Object[][] result = controller.getDb().getBooks();

        // Assert
        assertEquals(expectedName, result[0][1]);
        assertEquals(expectedISBN, result[0][2]);
        assertEquals(expectedShelf, result[0][3]);
        assertEquals(expectedSerialNumber, result[0][4]);
        assertEquals(expectedDate, result[0][5]);
    }

    @Test
    void RemoveBookTest() {
        // Arrange
        Controller controller = new Controller();

        int bookId = GenerateInt();
        String expectedName = GenerateString(7);
        String expectedShelf = GenerateString(7);
        String expectedISBN = GenerateString(7);
        String expectedSerialNumber = GenerateString(7);
        Date expectedDate = new Date(GenerateLong());
        Integer expectedUserId = GenerateInt();
        Book book = new Book(
                bookId,
                expectedName,
                expectedISBN,
                expectedShelf,
                expectedSerialNumber,
                expectedDate,
                expectedUserId
        );

        List<Book> books = new ArrayList<>();
        books.add(book);
        controller.setDb(books);

        // Act
        controller.getDb().removeRow(bookId);

        // Assert
        assertEquals(controller.getDb().getSize(), 0);
    }
}
